<?php

namespace Andering\Balikobot;

use GuzzleHttp;
use Andering\Balikobot\Definition\Carrier;

class Balikobot
{
	private $config;
	private $client;

	private $package = [
		'eid' => null,
		'order_number' => null,
		'real_order_id' => null,
		'service_type' => null,
		'branch_id' => null,
		'price' => null,
		'cod_price' => null,
		'cod_currency' => null,
		'vs' => null,
		'rec_name' => null,
		'rec_firm' => null,
		'rec_street' => null,
		'rec_city' => null,
		'rec_zip' => null,
		'rec_country' => null,
		'rec_email' => null,
		'rec_phone' => null,
		'return_track' => null
	];


	public function __construct($config)
	{

		$this->config = [
			'url' => 'https://api.balikobot.cz',
			'method' => 'POST',
			'auth' => []
		];

		$this->config = $config + $this->config;
	}

	public function getClient()
	{

		$this->client = new GuzzleHttp\Client($this->config);
		return $this->client;
	}

	private function call($carrier,$action,$data = [])
	{

		$client = $this->getClient();
		$r = $client->request($this->config['method'],implode("/",[$this->config['url'],$carrier,$action]),['json'=>$data]);
		$body = json_decode($r->getBody(), true);
		$body['status'] = isset($body['status']) ? $body['status'] : 200;

		switch($body['status'])
		{

			case 200:
				return $body;
				break;
			case 208:
				$e = new ExistPackageException("Položka s doloženým ID již existuje.");
				$e->setBody($body);
				throw $e;
				break;
			case 400:
				$e = new NotPlacedDataException("Operace neproběhla v pořádku, zkontrolujte konkrétní data.");
				$e->setBody($body);
				throw $e;
				break;
			case 403:
				$e = new NotActivatedCarrierException("Přepravce není pro použité klíče aktivovaný.");
				$e->setBody($body);
				throw $e;
				break;
			case 404:
				$e = new PackageNotExistException("Zásilka neexistuje, nebo již byla zpracována.");
				$e->setBody($body);
				throw $e;
				break;
			case 406:
				$e = new NoDataException("Nedorazila žádná data ke zpracování nebo nemůžou být akceptována.");
				$e->setBody($body);
				throw $e;
				break;
			case 409:
				$e = new ConflictDataException("Konfigurační soubor daného dopravce nebo profil není vyplněn/konflikt mezi přijatými daty u zásilky.");
				$e->setBody($body);
				throw $e;
				break;
			case 413:
				$e = new InvalidDataException("Špatný formát dat.");
				$e->setBody($body);
				throw $e;
				break;
			case 423:
				$e = new NotProductionException("Tato funkce je dostupná jen pro živé klíče.");
				$e->setBody($body);
				throw $e;
				break;
			case 501:
				$e = new NotImplementedException("Technologie toho dopravce ještě není implementována.");
				$e->setBody($body);
				throw $e;
				break;
			case 503:
				$e = new UnavailableCarrierException("Technologie dopravce není dostupná, požadavek bude vyřízen později.");
				$e->setBody($body);
				throw $e;
				break;
			default:
				$e = new ExceptionNotRecognizedException("Nepodařilo se rozeznat chybový stav.");
				$e->setBody($body);
				throw $e;
				break;

		}
	}


	public function add($carrier,$data) {
		$body =  $this->call($carrier,'add',$data);
		return array_filter($body,function($array){ return is_array($array); });
	}

	public function overview($carrier) {
		return $this->call($carrier,'overview');
	}

	public function orderview($carrier) {
		return $this->call($carrier,'orderview');
	}

	public function branches($carrier,$data) {
		return $this->call($carrier,"fullbranches/{$data['service_type']}");
	}

	public function services($carrier) {
		return $this->call($carrier,'services');
	}

	public function drop($carrier,$data)
	{
		return $this->call($carrier,'drop',$data);
	}

	public function order($carrier,$data)
	{
		return $this->call($carrier,'order',$data);
	}

	public function trackstatus($carrier,$data)
	{
		$body =  $this->call($carrier,'trackstatus',$data);
		return array_filter($body,function($array){ return is_array($array); });
	}


}

<?php

namespace Andering\Balikobot;

class LogicException extends \LogicException
{
	private $return;

	public function setBody($return)
	{
		$this->return = $return;
	}

	public function getBody()
	{
		return $this->return;
	}
}

class ExistPackageException extends LogicException
{

}

class NotPlacedDataException extends LogicException
{

}

class NotActivatedCarrierException extends LogicException
{

}

class PackageNotExistException extends LogicException
{

}

class NoDataException extends LogicException
{

}

class ConflictDataException extends LogicException
{

}

class InvalidDataException extends LogicException{

}

class NotProductionException extends LogicException
{

}

class NotImplementedException extends LogicException
{

}

class UnavailableCarrierException extends LogicException
{

}

class ExceptionNotRecognizedException extends LogicException
{

}

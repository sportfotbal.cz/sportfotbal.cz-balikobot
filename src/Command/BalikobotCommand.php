<?php

namespace Andering\Balikobot\Command;

use Symfony\Component\Console\Command\Command,
	Symfony\Component\Console\Input\InputInterface,
	Symfony\Component\Console\Output\OutputInterface,
	Symfony\Component\Console\Input\InputOption;

use Nette,
	Andering;

class BalikobotCommand extends Command {

	/** @var \Nette\DI\Container */
	private $container;

	/** @var array */
	private $config;

	public function __construct(array $config = [], Nette\DI\Container $container)
	{
		parent::__construct();

		$this->container = $container;
		$this->config = $config;
	}

	protected function configure()
	{
		$this->setName('Balikobot:export')
			->setDescription('Export product balikobot')
			->addOption('show', 's', InputOption::VALUE_NONE, 'Print available exports')
			->addOption('balikobot', 'f', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$show = $input->getOption('show');
		$balikobots = $input->getOption('balikobot');

		if ($show) {
			$output->writeln('Available exports:');

			foreach ($this->config['exports'] as $k => $v) {
				if ($v) {
					$output->writeln('- ' . $k);
				}
			}
		}

		$balikobots = $balikobots ?: array_keys($this->config['exports']);
		if (count($balikobots)) {
			foreach ($balikobots as $balikobot) {
				if (!isset($this->config['exports'][$balikobot]) || !$this->config['exports'][$balikobot]) {
					$output->writeln('Generator for ' . $balikobot . ' doesn\'t exist');
				}

				$generator = $this->container->getService('balikobot.' . $balikobot);

				$generator->save($balikobot . '.xml');
				$output->writeln('Balikobot ' . $balikobot . ' done');
			}
		}
	}
}
